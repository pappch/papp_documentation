Die Apps von pApp
===============

pApp wird sowohl für iOS (Apple-Geräte) wie auch für Androidgeräte angeboten.

Neben der herkömmlichen Verwendung auf Smartphones wurde pApp zusätzlich für Tablets optimiert und funktioniert auch damit hervorragend.

Die jeweilige Version (Tablet, Android, iOS) kann jeweils automatisch aus dem Apple Appstore für `iOS`_ oder Google Playstore für `Android`_ bezogen werden.

Auch sämtliche Updates laufen vollautomatisch über die entsprechenden Stores.


Aufbau
------

pApp ist wie PROFFIX modular aufgebaut - es können nur einzelne Module, kombinierte Module oder alle Module genutzt werden.

:doc:`address` kann also etwa ohne :doc:`rapporte` genutzt werden.

Welche Module für welchen Benutzer verfügbar sind kann in :doc:`pappconsole` detailliert konfiguriert werden.


.. note:: Die Modularität orientiert sich an PROFFIX. Je nach gewünschtem Modul kann ein anderes Modul also als Grundvoraussetzung benötigt werden. Details zu den Abhängigkeiten finden sich auch in :ref:`pxendpoints`


Vollständig nativ entwickelt
----------------------------

Sowohl die `Android`_ als auch die `iOS`_ Version von pApp werden komplett nativ mit den jeweiligen Entwicklungssuites der Hersteller entwickelt.

+------------+----------------+----------------------+--------------------+
| Hersteller | Betriebssystem | Entwicklungsumgebung | Enwicklungssprache |
+============+================+======================+====================+
| Google     | Android        | Android Studio       | Java / Android     |
+------------+----------------+----------------------+--------------------+
| Apple      | iOS            | Xcode                | Swift 3            |
+------------+----------------+----------------------+--------------------+

.. note:: Grundsätzlich wird pApp also zweimal entwickelt. Der Aufwand dieser parallelen und komplett nativen Entwicklung anstelle der Nutzung eines Frameworks ist damit zwar wesentlich höher, dafür verfügbt pApp aber über maximale Flexibilität, immer die neuesten Features und grösstmögliche Integration in das jeweilige Betriebssytem. 


Tabletmodus
-----------

pApp wurde speziell für Tablets optimiert. 

Vereinfacht ausgedrückt, bemerkt pApp wenn es auf einem Tablet ausgeführt wird und passt sich dann dem grösseren Bildschirm an (Multi Pane / Split View).

Im Modul :doc:`address` wird pApp z.B. **zweispaltig** und weist in der linken Spalte sämtliche Adressen aus während in der rechten Spalte die Details zur Adresse angezeigt werden.
Analog verhält sich pApp in anderen Modulen.


Um für Details trotzdem den **vollen Bildschirm zu nutzen**, kann der Tabletmodus mit folgenden Buttons am oberen Bildschirmrand jeweils **temporär ausgeschaltet** werden:

|pxtabletandroid| |pxtabletios|


Damit werden dann etwa die Adressdetails im Vollbild angezeigt.

Multilingual
------------

pApp ist multilingual wobei verschieden Sprachen bereits übersetzt sind (D,F,E,I...).
Die Sprache passt sich automatisch der vorgestellten Betriebssystemsprache an.

Zusätzliche Übersetzungen können auf Wunsch realisiert werden.

.. note:: Die **Leadsprache** in der Entwicklung von pApp ist Englisch. Neben der Kommunikation mit den jeweiligen Herstellern (Apple / Google) vereinfacht das auch die Eingliederung in den Quellcode.

Download
--------


|pxandroid|   `Android`_ 
|pxios|   `iOS`_                



Der Download der Apps ist **kostenlos**, zur Verbindung benötigten Sie aber sowohl die :doc:`install-pxapi` wie auch eine :doc:`pappid`.



.. _Android: https://play.google.com/store/apps/details?id=ch.pitw.pappofficial
.. _iOS: https://geo.itunes.apple.com/us/app/papp-das-app-fur-proffix/id1127456826?mt=8

.. |pxandroid| image:: _static/images/pApp-Android.png
	:scale: 70 %
	
.. |pxios| image:: _static/images/pApp-iOS.png
	:scale: 70 %
	
.. |pxtabletandroid| image:: _static/images/pxtablet-android.png
	:scale: 70 %
	
.. |pxtabletios| image:: _static/images/pxtablet-ios.png
	:scale: 70 %
	
