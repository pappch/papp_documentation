pApp Artikel
=============

`pApp Artikel`_ ist das Äquivalent zur `PROFFIX Artikel- und Lagerverwaltung`_.

Sämtliche von der `PROFFIX REST-API`_ unterstützen Funktionen sind mit `pApp Artikel`_ möglich. 
Auf sämtliche Artikel können die Funktionen :doc:`filter-group-search` angewendet werden.

.. _`pApp Artikel`: https://www.proffixapp.ch/features#Artikel
.. _`PROFFIX Artikel- und Lagerverwaltung`: http://www.proffix.net/Die-Software/Uebersicht-Modularitaet/Hauptmodule/Lagerverwaltung
.. _`PROFFIX REST-API`: http://www.proffix.net/Die-Software/Uebersicht-Modularitaet/Erweiterungen/PROFFIX-REST-API


Übersicht
---------

Um eine übersichtliche, schnell zu bedienende Oberfläche bereitzustellen sind in `pApp Artikel`_ die Informationen in die folgenden ein- und ausklappbaren Bereiche unterteilt:

- Generelle Details
- Lagerdetails
- Seriennummern
- Finanzbuchhaltung

Gleichzeitig werden die Felder **priorisiert**, d.h. nur die am häufigsten verwendeten Felder sind anfangs ersichtlich. Zusätzliche Felder (z.B. weitere Preise) können über die Schaltfläche **Mehr** eingeblendet werden.

|pxarticlelist|  |pxarticledetails|


Bestand
-------

Der Bestand eines Artikels wird in pApp bei aktiver Netzverbindung **live und automatisch abgeglichen**. Diese Abgleich geschieht beim Öffnen des jeweiligen Artikels (bzw. bei Verwendung).

|pxstock|

Ist keine Netzverbindung verfügbar, verwendet pApp den zuletzt synchronisierten Bestand (siehe Zeitstempel).


Suche mit Barcode
-----------------

In `pApp Artikel`_ können Artikel über ihren Barcode gesucht werden. 
Anstelle den Artikel nach Namen oder Artikelnummer zu suchen, kann einfach der Barcode der **Artikel- oder Seriennummer** eingescannt werden.

Sie finden den entsprechenden Button dazu direkt in der Suchleiste:

|pxarticlebarcodesearch|


.. note:: Damit der Artikel aufgrund des Barcodes gefunden wird müssen die Felder **Barcode** oder **Seriennummer** in PROFFIX abgefüllt sein.

   
.. |pxarticlelist| image:: _static/images/pxarticlelist.png
   :scale: 50 %
   :alt: pApp Artikel PROFFIX Android
   
.. |pxarticledetails| image:: _static/images/pxarticledetails.png
   :scale: 50 %
   :alt: pApp Artikel PROFFIX
   
.. |pxstock| image:: _static/images/pxstock-ios.png
   :scale: 80 %
   :alt: PROFFIX Artikel Bestand
   
.. |pxarticlebarcodesearch| image:: _static/images/px-barcode-search.png
   :scale: 80 %
   :alt: PROFFIX Artikel Barcode Suche