pApp-Console
============

Die pApp-Console ist das sichere Webinterface für die Verwaltung und Konfiguration von pApp.

In ihr können Kunden, Geräte und Benutzer einfach und übersichtlich verwaltet werden.

Benutzung
---------

Generell können Sie in pApp-Console sämtliche Einträge (z.B. Benutzer, Gerät) **provisorisch erstellen** und auch wieder löschen.

Ändern / Editieren
~~~~~~~~~~~~~~~~~~
Sämtliche Einträge mit dem Symbol |pxedit| können editiert und geändert werden.

Ausnahmen:

* Definitiv gesetzte Bestellungen
* Sämtliche Benutzer und Kunden die über eine aktive Lizenz verfügen

Löschen
~~~~~~~~~

Sämtliche Einträge mit dem Symbol |pxdelete| können gelöscht werden.

Ausnahmen:

* Definitiv gesetzte Bestellungen
* Sämtliche Benutzer und Kunden die über eine aktive Lizenz verfügen


.. warning:: Wenn Sie einen Kunden löschen, löschen Sie auch sämtliche auf ihn hinterlegte Benutzer und Geräte.


Wiederherstellung
~~~~~~~~~~~~~~~~~

Sollten Sie unabsichtlich einen Eintrag gelöscht haben, erstellen Sie diesen neu oder kontaktieren Sie den Support.


Zweifaktor-Authentifizierung
----------------------------
Die pApp-Konsole unterstützt die zweistufige Authentifizierung über das App Authy. Wir empfehlen, diese zu aktivieren.

**Vorgehen**

1. App **Authy** unter https://www.authy.com/app/mobile/ herunterladen
2. Mein **Profil** -> **Authentifizierung** aufrufen
3. **Mobiltelefonnummer** eingeben und **Einschalten** klicken

Anschliessend werden Sie bei jeder Anmeldung aufgefordert, den mit der App Authy generierten Code einzugeben.

Passwort vergessen
------------------

Sie können Ihr Passwort in der Loginmaske zurücksetzen ("Passwort vergessen").

Zugang gesperrt
---------------

pApp-Console verfügt über eine Reihe von fortschrittlichen Sicherheitsfeatures.
In Ausnahmefällen kann es vorkommen, dass Ihr Zugang automatisch gesperrt wird.

**Meldung "Zu viele Login Versuche. Versuchen Sie es bitte in 120 Sekunden."**

Sie haben zu oft versucht sich mit falschen Daten anzumelden. Bitte versuchen Sie es in 2 Minuten erneut

**Meldung "Ihr Benutzerkonto wurde von einem Administrator gesperrt."**

Ihr Zugang wurde manuell gesperrt. Gründe dafür sind:
* Verstoss gegen die Lizenzbedingungen
* Verstoss gegen die Nutzungbedingungen

Kontaktieren Sie bitte den Support für eine genaue Abklärung.

**Meldung "Zugang gesperrt"**

Diese Meldung kann eine Reihe von Gründen haben:

* Ihre IP-Adresse ist auf einer Blacklist (z.B. SPAM)
* Ihr System wurde von einem Schadprogramm infiziert
* Das System hat einen Angriff aus Ihrem Netzwerk festgestellt.

Kontaktieren Sie bitten den Support für mehr Informationen.



.. |pxedit| image:: _static/images/pxordersymbol.png
.. |pxdelete| image:: _static/images/pxdeletesymbol.png

