pApp Rapporte
=============

`pApp Rapporte`_ ist das Äquivalent zur `PROFFIX Leistungsverwaltung`_.

Sämtliche von der `PROFFIX REST-API`_ unterstützen Funktionen sind mit `pApp Rapporte`_ möglich.

.. _`pApp Rapporte`: https://www.proffixapp.ch/features#Rapporte
.. _`PROFFIX Leistungsverwaltung`: http://www.proffix.net/Die-Software/Uebersicht-Modularitaet/Zusatzmodule/Leistungsverwaltung
.. _`PROFFIX REST-API`: http://www.proffix.net/Die-Software/Uebersicht-Modularitaet/Erweiterungen/PROFFIX-REST-API

Übersicht
---------

Um eine übersichtliche, schnell zu bedienende Oberfläche bereitzustellen sind in `pApp Rapporte`_ die Eingabefelder in die folgenden ein- und ausklappbaren Bereiche unterteilt:

- Generelle Details
- Zeitdetails (wenn Leistung)
- Spesen (wenn Leistung)
- Artikeldetails (wenn Artikel)
- Lagerdetails (wenn Artikel)
- Finanzbuchhaltung

Gleichzeitig werden die Felder **priorisiert**, d.h. nur die am häufigsten verwendeten Felder sind anfangs ersichtlich. Für spezielle Felder (z.B. Vertreter, Rabatt) können über die Schaltfläche **Mehr** eingeblendet werden.

Leistungen
~~~~~~~~~~~~~

.. figure:: _static/images/pxrapportdetailtime.png
   :scale: 50 %
   :alt: pApp Leistungen Rapporte

   **Leistungen** können einfach erfasst werden.



Artikel
~~~~~~~~


.. figure:: _static/images/pxrapportdetailarticle.png
   :scale: 50 %
   :alt: pApp Leistungen Artikel

   Wird der Haken bei **Artikel** gesetzt, können sämtliche Artikel aus der `PROFFIX Artikel- und Lagerverwaltung`_ direkt für einzelne Kunden verrechnet werden.



.. _`PROFFIX Artikel- und Lagerverwaltung`: http://www.proffix.net/Die-Software/Uebersicht-Modularitaet/Hauptmodule/Lagerverwaltung



Neu erfassen und bearbeiten
---------------------------

Leistungen
~~~~~~~~~~

Um eine neue Leistung zu erfassen klicken Sie auf den + - Button. Befüllen Sie anschliessend die Felder mit den benötigten Informationen.


Artikel
~~~~~~~~~~
Um einen neuen Artikel zu erfassen klicken Sie auf den + - Button und aktivieren Sie die Artikeloption.
Befüllen Sie anschliessend die Felder mit den benötigten Adressinformationen.

.. Tip:: Um Artikel speditiver zu erfassen, können Sie den **Barcode der Produkt- oder Seriennummer** einscannen. Klicken Sie dazu auf den Button |pxscan|


.. |pxscan| image:: _static/images/pxscan.png
   :scale: 50 %
