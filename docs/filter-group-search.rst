Suchen, Gruppieren und Filtern
==============================

Da pApp Daten synchronisiert und in der geräteeigenen Datenbank ablegt (vgl. :doc:`synchronisation`) sind die Funktionen Suchen, Filtern und Gruppieren **sehr schnell**.

Diese Funktionen sind in sämtlichen pApp-Modulen wie etwa :doc:`address`, :doc:`rapporte` oder :doc:`article` aber auch in zahlreichen Hilfsmodulen verfügbar.


Suchen
------

pApp durchsucht **alle Logischen Felder** in den einzelnen Modulen. So können Sie z.B. in :doc:`address` auch direkt nach einem Kontakt suchen.

.. image:: _static/images/pxsearch-android.png
   :width: 45%
.. image:: _static/images/pxsearch-ios.png
   :width: 45%


Gruppieren
----------

Die Funktion Gruppieren ermöglicht das gruppieren von Einträgen. Sofern zählbare und logische Werte vorhanden sind (z.B. Stunden in :doc:`rapporte`) summiert pApp die Untereinträge auf der jeweiligen Gruppierung.

.. image:: _static/images/pxgroup-android.png
   :width: 45%
.. image:: _static/images/pxgroup-ios.png
   :width: 45%


Filtern
-------

Mittels Filter können **anspruchsvolle Suchläufe** durchgeführt werden. Mögliche Beispiele sind etwa:

- alle Adressen der Adressgruppe **Interessenten** in der **Region Zürich**
- alle Rapporte für den Auftrag **Systembetreuung** des Mitarbeiters **Müller Tim** nach dem 01.01.2017

Die gefilterten Einträge können anschliessend wiederum gruppiert, sortiert oder durchsucht werden.

.. image:: _static/images/pxfilter-android.png
   :width: 45%
.. image:: _static/images/pxfilter-ios.png
   :width: 45%