pApp Adressen
=============

Das `pApp Adressen`_ ist das Äquivalent zur `PROFFIX Adressverwaltung`_.

Sämtliche von der `PROFFIX REST-API`_ unterstützen Funktionen sind mit `pApp Adressen`_ möglich.

.. _`pApp Adressen`: https://www.proffixapp.ch/features#Adressen
.. _`PROFFIX Adressverwaltung`: http://www.proffix.net/Die-Software/Uebersicht-Modularitaet/Hauptmodule/Adressverwaltung
.. _`PROFFIX REST-API`: http://www.proffix.net/Die-Software/Uebersicht-Modularitaet/Erweiterungen/PROFFIX-REST-API

Übersicht
---------

Kommunikation
~~~~~~~~~~~~~

.. figure:: _static/images/pxaddresscommunication.png
   :scale: 50 %
   :alt: pApp Adressen Kommunikation

   Im Tab **Kommunikation** werden die verfügbare Kommunkationsmittel wie Telefon, Email und Adressdetails aufgelistet.



Kontakte
~~~~~~~~


.. figure:: _static/images/pxaddresscontacts.png
   :scale: 50 %
   :alt: pApp Adressen Kontakte

   Im Tab **Kontakte** befinden sich die für diese Adresse erfassten Kontakte.


.. figure:: _static/images/pxaddresscontactsdetails.png
   :scale: 50 %
   :alt: pApp Kontakte Details

   Für jeden Kontakt sind wiederum alle Kontaktdetails wie Telefon, Email und etwaige Adressdetails aufgelistet.


Notizen
~~~~~~~

.. figure:: _static/images/pxaddressnotes.png
   :scale: 50 %
   :alt: pApp Notizen

   Im Tab **Notizen** befinden sich die für diese Adresse bzw. Kontakt erfassten Notizen.

Neu erfassen und bearbeiten
---------------------------

Sie können in `pApp Adressen`_ Kontakte, Notizen und Adressen erfassen, kopieren, bearbeiten oder löschen.

Adressen
~~~~~~~~

Um einen neue Adresse zu erfassen klicken Sie auf den **+** - Button. Befüllen Sie anschliessend die Felder mit den benötigten Adressinformationen.

Sollten Sie mehr Felder benötigten (z.B.Postfach) können Sie diese über den Klick auf "Mehr" einblenden.

.. Tip:: Wenn Sie das Feld **Strasse** befüllen, ergänzt pApp die Felder Ort, Kanton, PLZ  und Land automatisch. Zusätzlich wird der Längen- und Breitengrad dieser Adresse auch direkt in PROFFIX synchronisiert (Geocoding)

Kontakte
~~~~~~~~

Um einen neuen Kontakt zu erfassen navigieren Sie zum Tab Kontakt und klicken auf den **+** - Button. Befüllen Sie anschliessend die Felder mit den benötigten Adressinformationen.

Wenn Sie die Optionen **Adresse gleich Hauptadresse** aktivieren, übernimmt pApp die Adresse der Hauptadresse.

Notizen
~~~~~~~~

Eine neue Notiz können Sie durch klicken auf den **+** - Button an verschiedenen Orten erstellen:

- Tab Notizen unter Adressen
- Tab Notizen unter Kontakte
- Aus der Telefonintegration


Telefonintegration
------------------

Die Telefonintegration gleicht ankommende Anrufe mit den synchronisierten Adressen aus der PROFFIX - Datenbank ab und zeigt direkt die passende Adresse / Kontakt an. 

.. figure:: _static/images/telefonintegration2.png
   :scale: 50 %
   :alt: pApp Adressen Telefonintegration

   Anzeige des einkommenden Anrufs mit Daten aus der PROFFIX Datenbank.
   
   
Entsprechende Anrufe bleiben nach Abschluss in der Taskbar - per Knopfdruck kann darauf direkt eine Notiz zur Adresse erstellt werden.

.. figure:: _static/images/telefonintegration.png
   :scale: 50 %
   :alt: Notizerstellung Adresse aus Telefonintegration

   Erfassen einer Notiz auf einkommenden Anruf
   

Aktivierung der Telefonintegration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wenn das Modul `pApp Adressen`_ vorhanden und die Telefonintegration in :doc:`pappconsole` aktiviert ist, erscheint unter Einstellungen zusätzlich folgende Option:

.. image:: _static/images/telefonintegration-enable.png
   :width: 80%

Damit kann die Telefonintegration jederzeit ein - oder ausgeschaltet werden.

.. note:: Die Telefonintegration ist aufgrund von Einschränkungen der Systemfunktionen seitens Apple / iOS nur für Androidgeräte möglich.
