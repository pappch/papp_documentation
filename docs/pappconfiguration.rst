Konfiguration pApp
==================


Voraussetzungen
---------------

Folgende **Voraussetzungen** sollten für die Konfiguration der PROFFIX REST-API mit pApp erfüllt sein:

* PROFFIX REST-API wurde installiert und konfiguriert (aktuellste Version)
* Firewall wurde angepasst / API kann öffentlich angesprochen werden
* SSL-Zertifikat wurde eingerichtet
* Passwort für PROFFIX REST-API wurde gesetzt

Diese Angaben sollten Sie also **bereithalten**:

* Hostname der REST-API (z.B. https://meinserver.dyndns.org oder https://restapi.meinedomain.ch)
* Portnummer der REST-API
* Passwort der REST-API


Kunde erstellen
---------------

1. Navigieren Sie in der pApp-Console zu **pApp Kunden**.
2. Klicken Sie auf Kunde hinzufügen
3. Befüllen Sie das Formular wie folgt:

* **pApp-ID:** Leicht merkbarer Kurzslug (z.B. musterag)
* **pURL:** Entspricht dem Hostnamen inkl. dem Port und der Endung /pxapi/v1 (z.B. https://meinserver.dyndns.org:666/pxapi/v2/)
* **PROFFIX REST-API Key:** Das in der REST-API vergebene Passwort
* **Adresse:** Adresse des Endnutzers


Anschliessend kann der Kunde über den Button **Neuen Kunden erstellen** gespeichert werden.

.. Tip:: Nachdem Sie die Eingaben gespeichert haben, können Sie die korrekte Konfiguration der PROFFIX REST-API über den Button **Check** überprüfen.

.. note:: Die pURL muss zwingend mit **/pxapi/v2/** enden. Bei einem Versionwechsel der API (z.B. auf v3) kann dann nur noch dieser Parameter angepasst werden.


Benutzer erstellen
------------------

Nun müssen dem Kunden noch die entsprechenden Benutzer hinzugefügt werden.
Diese Benutzer sind analog zu den Benutzern aus PROFFIX.

1. Navigieren Sie in der pApp-Console zu **pApp Benutzer**
2. Klicken Sie auf **Benutzer hinzufügen**
3. Wählen Sie den zugehörigen **pApp Kunden** aus
4. Geben Sie den **Benutzernamen aus PROFFIX** (= Kurzzeichen) ein
5. Idealerweise ergänzen Sie **Vor- und Nachnamen** des Benutzers. Diese Daten werden nicht verwendet, erleichtern aber die Orientierung.

Gerät registrieren
------------------

Abschliessend muss das mobile Gerät in der pApp-Console registriert werden.

1. Installieren Sie **pApp PX**  (`Android`_ / `iOS`_) auf Ihrem mobilen Gerät.
2. Klicken Sie auf **Anmelden**
3. Geben Sie den erstellten **Benutzernamen** sowie die erstellte **pApp-ID** an.

Sie erhalten eine Rückmeldung, dass das Gerät registriert ist.

Anschliessend können Sie dem Benutzer die Lizenzen zuweisen.

.. _Android: https://play.google.com/store/apps/details?id=ch.pitw.pappofficial
.. _iOS: https://geo.itunes.apple.com/us/app/papp-das-app-fur-proffix/id1127456826?mt=8

Vereinfachte Registrierung (Optional)
-------------------------------------

Wenn Smartlock (:doc:`pappid`) nicht aktiviert ist, kann die Benutzer und Geräteregistrierung auch über das App erfolgen.

Dazu lädt der Endbenutzer pApp aus dem entsprechenden Store herunter und loggt sich mit der pApp-ID, seinem Benutzernamen und Passwort ein.
pApp erstellt den Benutzer in der pApp-Console dann automatisch.


PROFFIX Adressen Geocodieren (Optional)
---------------------------------------

pApp benötigt zur Anzeige der Adressen auf der Karte die Geokoordinaten. Bei Bearbeitung / Erstellung von Adressen durch pApp werden diese
automatisch abgefüllt. Bei den bereits in der Datenbank vorhandenden Adresseinträgen fehlen diese aber.
Um sämtliche Adresseinträge in der PROFFIX Datenbank einmalig mit Geokoordinaten zu versehen, stellen wir folgendes Tool zur Verfügung:

 `Go PROFFIX Geocode auf Github <https://github.com/pitwch/go-proffix-geocode>`_.

Beachten Sie die entsprechenden Anweisungen auf Github.


