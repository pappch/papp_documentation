Systembezogenes Design
----------------------

Sowohl Apple wie auch Google haben ganz unterschiedliche Auffassungen von Design und Funktionalität. pApp richtet sich in den jeweiligen Versionen in erster Linie an eben diesen Designrichtlinien aus - mit einigen Anpassungen:

Eingabe von Zahlen
------------------
Standardmässig werden in iOS / Android Ziffern und Zahlen in Feldern angehängt. pApp übersteuert dieses Verhalten und leert die Felder jeweils vor der Eingabe. Um also etwa die Ziffer **21** einzugeben (z.B. für Preis)
müssen also nicht zuerst die Werte 0.0 gelöscht werden, sondern es kann direkt **21** eingegeben werden.

Soll hingegen der Dezimalwert **21,54** eigegeben werden, muss das Zeichen **.** auf der Tastatur verwendet werden.

Eingabe bestätigen
------------------
Vor allem auf Apple / iOS ist das Bestätigen von Eingabefeldern bei vielen Feldern zum Teil mühsam.

pApp verfügt über eine **modifizierte Eingabetastatur**, die diesen Schritt vereinfacht.

|pxcustomkeyboard|

Sobald die Eingabe in ein Feld abgeschlossen ist, klicken Sie den Button **Done** und fahren Sie mit dem nächsten Feld fort.

Kopieren und Löschen
--------------------
Die Funktionen Löschen und Kopieren stehen in pApp mittels Swipe (wischen von Rechts nach Links) in zahlreichen Modulen zur Verfügung.

|pxcopydelete|

So können etwa Rapporte, Aufträge oder Adressen einfach kopiert und bearbeitet werden.


.. |pxcustomkeyboard| image:: _static/images/pxcustomkeyboard-ios.png
   :scale: 50 %
   :alt: pApp Modifizierte Eingabetastatur
   
.. |pxcopydelete| image:: _static/images/pxdeletecopy-ios.png
   :scale: 50 %
   :alt: pApp Kopieren und Löschen