PROFFIX REST-API
=============



Installation
---------------

Sie können die PROFFIX REST-API direkt von `PROFFIX <https://www.proffix.net/>`_ bzw. Ihrem PROFFIX-Partner beziehen. Ebenfalls erhältlich ist eine detaillierte Installationsanleitung für die REST-API.

Idealerweise wird die PROFFIX REST-API auf einem Server installiert, der 24 x 7 h online ist.


SSL-Zertifikat
---------------

Für die Nutzung von pApp muss die PROFFIX REST-API **zwingend mit einem gültigen SSL-Zertifikat** eingerichtet werden.

**Gültig bedeutet:**

- Zertifikat ist **nicht abgelaufen**
- Zertifikat kann über eine **öffentliche Zertifizierungsstelle** geprüft werden (keine selbstsignierten Zertifikate!)
- Zertifikat **passt zur pURL** (z.B: https://meinserver.dyndns.org)

Falls ein SSL-Zertifikat bereits in irgendeiner Form vorhanden ist, kann dasselbe auch für pApp / PROFFIX REST-API verwendet werden (z.B. von VPN-Gateway,etc.)

Falls noch kein Zertfikat vorhanden ist - mit folgenden Zertifikaten haben wir gute Erfahrungen gemacht:

- Comodo PositiveSSL
- Comodo InstantSSL
- Comodo PremiumSSL

.. note:: Das Zertifikat dient in erster Linie der Verschlüsselung des Datenverkehrs. **Günstige Standardzertifikate** sind für diesen Zweck absolut ausreichend.

.. _pxlicence:

Lizenzierung PROFFIX REST-API
---------------

Für die Nutzung der REST-API von PROFFIX müssen Sie über eine gültige PROFFIX REST-API Lizenz verfügen.

.. image:: _static/images/pxinstall_licence.png
   :scale: 80 %
   :alt: PROFFIX REST-API Lizenzierung
   
Die entsprechende Abrechnung sowie weitere Informationen zur PROFFIX REST-API erfolgt bzw. erhält man direkt über `PROFFIX <https://www.proffix.net/>`_.

.. note:: Da pApp keine kontiniuierliche Verbindung zur PROFFIX REST-API aufrechterhält, können theoretisch mehrere Benutzer eine Lizenz verwenden. 
Sollte diese besetzt sein, zeigt pApp eine entsprechende Warnung an.

.. _pxendpoints:

Endpunkte / Abhängigkeiten
--------------------------

Folgende Übersicht zeigt eine Übersicht über die in der PROFFIX REST-API freizuschaltenden Endpunkte.

+-----------------+-------------------+------------------------------+
| pApp Modul      | Funktion          | PROFFIX REST-API             |
+=================+===================+==============================+
| :doc:`address`  | Nur Lesen         | ---                          |
+-----------------+-------------------+------------------------------+
| :doc:`address`  | Schreiben + Lesen | Adressverwaltung             |
+-----------------+-------------------+------------------------------+
| :doc:`article`  | Nur Lesen         | ---                          |
+-----------------+-------------------+------------------------------+
| :doc:`article`  | Schreiben + Lesen | Artikel- und Lagerverwaltung |
+-----------------+-------------------+------------------------------+
| :doc:`rapporte` | Schreiben + Lesen | Leistungsverwaltung          |
+-----------------+-------------------+------------------------------+



Optimierung Session-Timeout
---------------

.. Tip:: Ab Version 1.2 gibt pApp die Lizenz nach erfolgreicher Synchronisation automatisch wieder frei. Eine Optimierung des Session-Timeout ist damit nicht mehr nötig.


Sie können in der PROFFIX REST-API den Wert für den Session-Timeout auf einen tieferen Wert einstellen. 

.. image:: _static/images/pxsessiontimeout.png

Der ideale Wert berechnet sich aus folgender Überlegung:

- Wie lange braucht eine Abfrage der Daten durch pApp maximal?
- Wie lange braucht eine solche Abfrage bei schlechter Verbindung?
- Wie gross ist der abzufragende Datenbestand?

Grundsätzlich muss der "ideale Wert" ausprobiert werden. Die Grössenordnung wird je nach Geschwindigkeit, Datenbankgrösse und Abfrage zwischen 20 - 300 Sekunden liegen.


