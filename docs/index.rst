Dokumentation pApp - das App für PROFFIX
========================================

`pApp`_ sorgt dafür, dass Sie Daten aus `PROFFIX`_ auch unterwegs und mobil nutzen können. `pApp`_ wird vollständig nativ sowohl für `iOS`_ wie auch für `Android`_ entwickelt.


.. _pApp: https://www.proffixapp.ch/
.. _PROFFIX: https://www.proffix.ch/
.. _Android: https://play.google.com/store/apps/details?id=ch.pitw.pappofficial
.. _iOS: https://geo.itunes.apple.com/us/app/papp-das-app-fur-proffix/id1127456826?mt=8

In dieser Dokumentation finden Sie immer die aktuellsten Funktionen und Features von `pApp`_


.. note:: Wir verwenden für Screenshots in dieser Dokumentation die Screens von Androidgeräten. Der Aufbau für die iOS - Version von pApp ist aber weitgehend ähnlich - entsprechende Ausnahmen werde klar erwähnt.


Übersicht
==============

pApp besteht aus verschiedenen Komponenten:

* :ref:`apps-docs`
* :ref:`modules-docs`
* :ref:`rest-docs`
* :ref:`console-docs`


.. _apps-docs:

.. toctree::
   :maxdepth: 2
   :caption: pApp Dokumentation

   apps
   compatibility
   design
   security
   synchronisation
   filter-group-search
   offline-mode
   errorhandling

.. _modules-docs:

.. toctree::
   :maxdepth: 2
   :caption: pApp Module
   
   homescreen
   address
   rapporte
   article
   tools
   
.. _rest-docs:

.. toctree::
   :maxdepth: 2
   :caption: REST-API Dokumentation
   
   restapi
   install-pxapi

.. _console-docs:

.. toctree::
   :maxdepth: 2
   :caption: pApp-Console Dokumentation
   
   pappid
   pappconsole
   pappconfiguration

