Fehlerbehebung
==============

In folgendem Abschnitt finden Sie mögliche Lösungen zur Fehlerbehebung

pApp öffnet und schliesst gleich wieder
---------------------------------------

Versuchen Sie den Task von pApp zu beenden.

Unter Android: Taskbutton ("linker Button" anklicken) und auf "Alle Beenden" klicken

Unter iOS: Homebutton 2x drücken, pApp Task "nach oben" wegwischen

pApp-ID falsch oder nicht korrekt
---------------------------------

Achten Sie darauf, dass bei der pApp-ID kein abschliessender Leerschlag (z.B. "demo ") vorhanden ist.

pApp synchronisiert nicht alles
-------------------------------

Wenn einzelne Einträge (z.B. Notizarten) in pApp fehlen sind mit hoher Wahrscheinlichkeit die **Berechtigungen in PROFFIX unzureichend** gesetzt.

Unter **Einstellungen --> Datenbank synchronisieren** zeigt pApp die fehlerhaften Tabellen / Hilfstabellen an.
Korrigieren Sie diese in PROFFIX --> Diverses --> Benutzerverwaltung.