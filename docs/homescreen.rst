pApp Homescreen
===============

Der Homescreen von pApp ist die Startoberfläche. Sämtliche Module, Hilfsmodule und Zusatzfeatures können hier
aufgerufen werden.

Über die Suchfunktion kann der Homescreen einfach nach den gewünschten Funktionen / Informationen durchsucht werden.


|pxhomescreenandroid|  |pxhomescreenios|


Hauptmodule
-----------

Äquivalent zu PROFFIX Hauptmodulen (z.B. Adressen, Artikel...)


Hilfsmodule
-----------

Enthält Hilfstabellen aus PROFFIX (z.B. Aufträge, Notizen...)


Universal Search
----------------

Mit der Suchfunktion in der Fusszeile des Homescreen lässt sich die Universal Search starten.
Diese durchsucht **sämtliche in pApp synchronsierten PROFFIX Module** und Hilfsmodule und zeigt sie an.

Beim Auswählen eines Suchergebnisses wird der Benutzer direkt ins passende Module weitergeleitet.


|pxuniversalsearchandroid|  |pxuniversalsearchios|


Tools
-----

Unter Tools befinden sich **spezielle Funktionen** von pApp (z.B. Barcode - Scan, Karten von Adressen).

.. note:: Die Tools sind auch in andereren Modulen integriert - über den Homescreen können Sie aber direkt aufgerufen werden.


Einstellungen
-------------

Unter Einstellungen kann pApp angepasst werden (z.B. Details zur :doc:`synchronisation` oder zu :doc:`security`


.. |pxhomescreenandroid| image:: _static/images/px-dashboard-android.png
   :scale: 60 %
   :alt: pApp Homescreen Android
   
.. |pxhomescreenios| image:: _static/images/px-dashboard-ios.png
   :scale: 60 %
   :alt: pApp Homescreen iOS

.. |pxuniversalsearchandroid| image:: _static/images/px-universal-search-android.png
   :scale: 60 %
   :alt: pApp Universal Search Android
   
.. |pxuniversalsearchios| image:: _static/images/px-universal-search-ios.png
   :scale: 60 %
   :alt: pApp Universal Search iOS