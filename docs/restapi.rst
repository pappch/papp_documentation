REST-API
========================

Zu Verbindung von pApp und PROFFIX wird eine REST-API benötigt ( ~ sichere und fürs Web optimierte Schnittstelle).

pApp unterstützt(e) derer zwei:

- pApp PX: Offizielle REST-API von PROFFIX (aktuell)
- pApp DF: Infoffizielle REST-API von Dreamfactory (nicht mehr unterstützt)

pApp PX verwendet nur noch **die offizielle PROFFIX REST-API**.


Wieso PROFFIX REST-API?
-----------------------

Das pApp zwei REST-APIs unterstützt ist **historisch bedingt**. Bei der Entwicklung der ersten Version von pApp DF (DF steht für Dreamfactory) existierte die PROFFIX REST-API noch nicht; als Alternative wurde `Dreamfactory`_ verwendet.
Damit wurde direkt mit der Datenbank von PROFFIX interagiert - ein Vorgang der sehr aufwendig zu warten und immer mit einem Restrisiko bezüglich Fehlmanipulationen behaftet war.

Mit der Verwendung der **PROFFIX REST-API** stellen wir sicher, dass Daten in und aus PROFFIX **immer zu 100%** kongruent sind und nur ein sehr geringer Wartungsaufwand ensteht.
pApp PX führt keine manuellen Datenbankbewegungen aus sondern **nutzt für sämtliche Lese- und Schreibaktionen die PROFFIX REST-API**.


.. caution:: Die **nicht mehr unterstützen Apps zu pApp DF** befinden sich zu Anschauungszwecken noch in den Stores (`Android`_ / `iOS`_). Sie werden aber nicht mehr aktualisiert.


.. _Dreamfactory: https://www.dreamfactory.com/
.. _Android: https://play.google.com/store/apps/details?id=ch.pitw.papp
.. _iOS: https://itunes.apple.com/ch/app/papp-das-app-f%C3%BCr-proffix-df/id1082767566?mt=8
