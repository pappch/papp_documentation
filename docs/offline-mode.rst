Offline - Modus
================

Der Offline - Modus von pApp ist eine spezielle Form der Synchronisation, welche **vollständige Unabhängigkeit von Netzverbindungen (Internet)** ermöglicht.
Der Ablauf in pApp ist komplett automatisiert.

Mögliche Szenarien
------------------

**Beispiel 1:** Ein Mitarbeiter eines Unternehmens für Wartung arbeitet häufig in sensitiven Räumen welche über keine oder nur sehr schlechte Netzverbindungen verfügen.

Mit aktiviertem Offline - Modus kann dieser Mitarbeiter wie gewohnt Rapporte (sogar mit Artikeln) erfassen - sobald er wieder über eine Netzverbindung verfügt synchronisiert pApp die erfassten Rapporte automatisch mit PROFFIX.

**Beispiel 2:** Ein Unternehmen möchte dass Servicemitarbeiter von unterwegs Rapporte erfassen können. Sie stellen den Mitarbeitern dazu ein Tablet zu Verfügung, wollen aber keinen zusätzlichen Vertrag für einen mobilen Internetzugang.

Das Tablet wird vor und nach Arbeitsbeginn jeweils am Unternehmenssitz aufgeladen. An diesem Standort ist auch ein Wireless-Netzwerk verfügbar über welches pApp automatisch eine Synchronsierung durchführt. Durch den ganzen Tag hindurch erfassen
die Mitarbeiter Rapporte im Offline - Modus.
Nach Arbeitsschluss lagern die Mitarbeiter das Tablet jeweils wieder am Unternehmenssitz - pApp synchronisiert dann automatisch per WLAN sämtlche über den Tag erfassten Rapporte.


Synchronisationsfehler
----------------------

todo
