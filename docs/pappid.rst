pApp-ID
=======

Die pApp-ID ist ein Identifikationsmerkmal und erfüllt mehrere Funktionen:

Vorteile
--------
Anstatt jedem Benutzer den API-Key,die URL der REST-API sowie den Port mitzuteilen, genügt die Bekanntgabe der leicht einprägsamen pApp-ID.

Diese stellt sämtlichen notwendigen Informationen über einen sicheren Kanal bereit.

Gleichzeitig dient die pApp-ID zur sicheren Identifikation und Vereinfacht die Anmeldung mit mehreren Geräten.


Verknüpfung pApp und REST-API
-----------------------------
Die pApp-ID verbindet die PROFFIX bzw. Dreamfactory REST-API mit den verschiedenen Versionen von pApp.

Dabei werden über die Lizenzserver von pApp **immer nur Lizenz- und Hilfsinformationen** ausgetauscht.  

.. note:: Sämtliche **sensiblen und persönlichen Geschäftsdaten** werden ohne Umwege direkt von pApp auf die REST-API übermittelt.
		  :doc:`pappconsole` speichert weder PROFFIX-Benutzerpasswörter noch Daten aus PROFFIX und hat keinen direkten Zugriff auf die PROFFIX-Datenbank.


**Datenübermittlung und Verwendung (Details)**


+----------------------------+-------------------------+
| Daten                      | Ziel                    |
+============================+=========================+
| PROFFIX - Daten            | REST-API                |
+----------------------------+-------------------------+
| PROFFIX - Benutzerpasswort | REST-API                |
+----------------------------+-------------------------+
| PROFFIX - Benutzername     | REST-API / pApp-Console |
+----------------------------+-------------------------+
| REST-API Key               | REST-API / pApp-Console |
+----------------------------+-------------------------+
| Gerätetoken                | pApp-Console            |
+----------------------------+-------------------------+
| pApp - ID                  | pApp-Console            |
+----------------------------+-------------------------+



Smartlock
---------
Das Feature Smartlock kann für jeden Kunden einzeln aktiviert werden. 

Sofern aktiviert, akzeptiert die pApp-ID nur bereits bekannte Geräte - / Benutzerinformationen. Auf alle anderen Kombinationen wird nicht reagiert.

Wenn Smartlock aktiviert ist, können neue Geräte **nicht registriert werden**.


SelfDelete
----------
Das Feature SelfDelete kann für jeden Kunden einzeln aktiviert werden.

Wenn SelfDelete eingeschaltet ist, können Benutzer mit zuvielen Geräten Ihr altes Gerät direkt aus pApp auf inaktiv setzen bzw. die Registrierung löschen.