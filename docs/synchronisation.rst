Synchronisierung
================

pApp liest Daten nicht live sondern synchronisiert die geräteinterne Datenbank mit der PROFFIX - Datenbank. 

Was genau synchronisiert wird ist **immer abhängig von den in pApp bzw. PROFFIX verwendeten Modulen** - wobei auch diese mittels :ref:`syncfilters` individualisiert werden können.




Vorteile der Synchronisierung
-----------------------------

Durch die technisch aufwendigere und individualisierbare Synchronisierung ergeben sich einige Vorteile:

- Bessere Performance
- Netzunabhängige Nutzung / Offlinenutzung
- Diverse Möglichkeiten Daten auf dem Gerät interaktiv zu nutzen
- Anreicherung PROFFIX - Daten mit Metadaten (z.B. Geodaten)

Die synchronisierte Datenmenge ist abhängig vom verwendeten PROFFIX Mandanten und vom eingestellten :ref:`syncinterval`.

Arbeitsmöglichkeiten
--------------------

Folgendes Schema zeigt die Arbeitsmöglichkeiten mit pApp in Abhängigkeit der Internet-/Netzverbindung.

+-----------------------+---------------------------------------+--------------------+ 
| #                     | Daten lesen                           | Daten schreiben    | 
+=======================+=======================================+====================+
| Mit Internetzugriff 	| Interne Datenbank / Synchronisation   | Direkt in PROFFIX  | 
+-----------------------+---------------------------------------+--------------------+ 
| Ohne Internetzugriff 	| Interne Datenbank/:doc:`offline-mode` | :doc:`offline-mode`|
+-----------------------+---------------------------------------+--------------------+ 


Daten können zudem mittels Sync-Filters eingeschränkt werden.

.. _syncinterval:

Synchronisationsintervall
-------------------------
Das Synchronisationsintervall kann auf jedem Gerät unter Einstellungen individuell eingestellt werden.

.. note:: Teilweise können "Hilfsapps" welche z.B. den Stromverbrauch senken sollten die planmässige Synchronisation verhindern. Definieren Sie pApp als in diesem Fall als Ausnahme oder verwenden Sie die :ref:`manualsync`.

.. _syncfilters:

Sync Filters
------------
Je nach Benutzer können gewisse Bereiche von PROFFIX (z.B. Rapporte) sehr gross sein. 
Obwohl pApp technisch tausende von Einträge problemlos verkraftet, macht es in den meisten Fällen keinen Sinn, alte oder nicht mehr aktuelle Einträge zu synchronsieren.


.. image:: _static/images/pxsyncfilters.png
	:scale: 50 %


Dazu können Sie unter Einstellungen -> Synchronisation beim entsprechenden Modul einen sog. **Sync Filter** setzten.

Dieser beschränkt die Synchronisation der Positionen auf den gefilterten Bereich.


.. _manualsync:

Manuelle Synchronisation
------------------------
pApp bietet eine Reihe von Möglichkeiten um Daten manuell zu synchronisieren.
Dabei wird Rücksicht auf die **typischen Gesten des jeweiligen Herstellers** genommen.


Komplettsynchronisation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unter Einstellungen -> Synchronisieren kann eine manuelle Komplettsynchronisation vorgenommen werden.

Dabei werden **sämtliche Daten und Hilfstabellen** neu synchronisiert.

.. _quicksync:

Schnellsynchronisation (Quicksync)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Die typische Geste dazu ist, den Finger am oberen Bildschirmrand anzusetzen und bis etwa in die Mitte desselben zu ziehen.

Dabei wird nur die aktuelle Ansicht (z.B. Rapporte) ohne Hilfstabellen (z.B. Mitarbeiter) aktualisiert.

Löschen der internen Datenbank
------------------------------
In der internen Datenbank von pApp werden Geschäftsdaten sicher verwahrt.
Trotzdem kann es einmal vorkommen, dass diese Remote gelöscht werden muss (z.B. Diebstahl, Neuorientierung Mitarbeiter)

Die interne Datenbank von pApp wird deshalb in folgenden Fällen **automatisch gelöscht**:

- Lizenz ist abgelaufen
- Benutzer existiert in PROFFIX nicht mehr
- PIN für die Datenbankverschlüsselung wird 3x falsch eingegeben

Ein erneutes Anmelden in pApp mit einem gültigen Zugang sychronisiert die interne Datenbank neu.

Roaming
-------
Je nach Kunde und Synchronisationsintervall, kann die Nutzung der REST-API und pApp erheblichen Datenverkehr verursachen.

Insbesondere für längere Aufenthalte im Ausland empfehlen wir, Autosync in pApp zu deaktivieren oder zumindest das
:ref:`syncinterval` zu reduzieren. :ref:`quicksync` ist auch dann noch möglich.