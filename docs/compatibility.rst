Kompatibilität
==============

Changelog
------------

Grundsätzlich ist pApp immer mit der **aktuellsten Version der PROFFIX REST-API** kompatibel.

Die genaue Kompatibilität zu jeweiligen Version von pApp kann dem `Changelog  <https://www.proffixapp.ch/changelog>`_
entnommen werden. 

Gerätekompatibilität
--------------------
pApp ist mit einem Grossteil sämtlicher **Android- und iOS Geräte** kompatibel.


**iOS / Apple:**  Sämtliche Geräte ab iOS 10 (bei älteren Versionen Demo ausprobieren)

**Android:**  Geräte ab Android 4.2 (ca. 10'000 Geräte)


Grundsätzlich gilt; Wenn Sie pApp im entsprechenden Store betrachten können, ist es auch mit Ihrem Gerät kompatibel.

Sollte pApp nicht für Ihr Gerät im Store erscheinen, kontaktieren Sie bitte den Support.

.. note:: Trotzdem dass pApp auch mit vielen älteren Betriebssystemen kompatibel ist, wird dringend empfohlen das Betriebssystem des mobilen Gerätes immer aktuell zu halten.

Manueller Override
------------------

Sollten Sie pApp mit einer nicht offiziell kompatiblen Version der PROFFIX REST-API verwenden wollen, haben Sie die Möglichkeit, den Kompatibilitätscheck in pApp zu umgehen.

Der entsprechende **Override-PIN** ist jeweils eine Monat gültig und befindet sich in der Detailansicht des jeweiligen pApp-Kunden in pApp-Console.