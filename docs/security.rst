Sicherheit
==========

PROFFIX Login
-------------

Die PROFFIX Login Daten werden nach der einmaligen Anmeldung direkt auf dem Gerät gespeichert.

Die Speicherung erfolgt dabei direkt in **betriebssystemeigenen und sicheren Schlüsselspeicher** (Keychain in iOS,  Account Manager in Android).

|pxloginandroid|  |pxloginios|


Das Login - Passwort wird jeweils **nur an die PROFFIX REST-API** übertragen.


Verschlüsselung
---------------

Die **interne Datenbank von pApp** kann mittels **AES-256 verschlüsselt werden**.
Die Verschlüsselung kann jederzeit unter **Einstellungen** aktiviert oder deaktiviert werden.

|pxencryption|

Bei aktivierter Verschlüsselung erscheint beim Beenden des Tasks von pApp oder beim Neustart des Gerätes ein Lock-Screen der zur Eingabe des PINs auffordert.

|pxencryptionscreen| 

.. note:: Die interne Datebank von pApp wird nach dreimaliger Falscheingabe des PINs automatisch gelöscht. Sie müssen sich anschliessend erneut anmelden.


Berechtigungen
--------------

pApp benötigt diverse Systemberechtigungen - sowohl auf iOS wie auch auf Android. Diese werden automatisch bei der Erstinstallation gesetzt.

Es wird **dringend empfohlen, sämtliche Berechtigungen zu erteilen** - pApp benötigt diese für einen reibungslosen Betrieb und nutzt sie **nur für geräteinterne Zwecke**.

Werden Berechtigungen nicht erteilt, führt dies zu ungewolltem Verhalten oder fehlenden Funktionen.

|pxpermissionandroid| |pxpermissionios|

   
.. |pxencryption| image:: _static/images/pxencryption.png
   :scale: 80 %
   :alt: pApp Verschlüsselung Datenbank
   
.. |pxencryptionscreen| image:: _static/images/pxencryption-screen.png
   :scale: 60 %
   :alt: pApp Lock-Screen
   
.. |pxloginandroid| image:: _static/images/pxlogin-android.png
   :scale: 60 %
   :alt: PROFFIX REST-API Login
   
.. |pxloginios| image:: _static/images/pxlogin-ios.png
   :scale: 60 %
   :alt: PROFFIX Login Daten
   
.. |pxpermissionandroid| image:: _static/images/px-permissions-android.png
   :scale: 60 %
   :alt: PROFFIX Login Daten
   
.. |pxpermissionios| image:: _static/images/px-permissions-ios.png
   :scale: 60 %
   :alt: PROFFIX Login Daten